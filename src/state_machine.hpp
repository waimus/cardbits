#pragma once

#include <memory>
#include <stack>

#include "state.hpp"

namespace Seraphus {
typedef std::unique_ptr<State> StateRef;

/// StateMachine class manages the states of the program in a stack
class StateMachine {
public:
  StateMachine();
  ~StateMachine();

  /// Mark current state to be removed, any changes will pe processed
  /// automatically in the StateMachine::ProcessStateChanges()
  void AddState(StateRef new_state, bool is_replacing = true);

  /// Mark current state to be removed, any changes will pe processed
  /// automatically in the StateMachine::ProcessStateChanges()
  void RemoveState();

  /// Process any changes to the states stack made via StateMachine::AddState
  /// or StateMachine::RemoveState()
  void ProcessStateChanges();

  /// Retrieve the currently running state
  StateRef &GetActiveState();

private:
  std::stack<StateRef> _states;
  StateRef _newState;

  bool _is_removing;
  bool _is_adding;
  bool _is_replacing;
};
} // namespace Seraphus
