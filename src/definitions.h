#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 720
#define WINDOW_TITLE "Cardbits"

#define BACKGROUND_COLOR sf::Color(24, 0, 47, 255)

#define COLOR_ENVIOUS_R sf::Color(255, 34, 68, 255)
#define COLOR_ENVIOUS_G sf::Color(51, 255, 153, 255)
#define COLOR_ENVIOUS_B sf::Color(119, 34, 255, 255)
#define COLOR_ENVIOUS_W sf::Color(255, 204, 0, 255)

#endif
