#include <cstdint>
#include <iostream>
#include <sstream>

#include "../definitions.h"
#include "SFML/Graphics/Sprite.hpp"
#include "SFML/Graphics/Text.hpp"
#include "SFML/Window/Event.hpp"
#include "SFML/Window/Keyboard.hpp"
#include "default_state.hpp"
#include "ingame_state.hpp"

InGameState::InGameState(GameDataRef data) : _context(data) {}
InGameState::~InGameState() { _context.reset(); }

void InGameState::Initialize() {
  _context->window->setTitle("Current state: DefaultState");

  _context->assets_manager->LoadFont(
      "Patrick Hand SC", "../assets/fonts/Font_OFL_Patrick_Hand_SC.ttf");
  _context->assets_manager->LoadTexture("T_Card_Square",
                                        "../assets/textures/T_Card_Square.png");

  compose_texts();
  compose_interactible_bits();
}

void InGameState::HandleInput() {
  sf::Event event;
  while (_context->window->pollEvent(event)) {
    if (event.type == sf::Event::Closed) {
      _context->window->close();
    }

    if (event.type == sf::Event::KeyPressed) {
      if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
        _context->window->close();
      }

      if (event.type == sf::Event::Resized) {
        auto view = _context->window->getDefaultView();
        view.setSize(static_cast<float>(event.size.width),
                     static_cast<float>(event.size.height));
        _context->window->setView(view);
      }

      if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) {
        _context->state_machine->AddState(
            std::make_unique<DefaultState>(_context), true);
      }

      if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) {
        std::cout << "You are already in SecondaryState\n";
      }
    }
  }
}

void InGameState::Update(sf::Time delta_time) {}

void InGameState::Draw() {
  _context->window->clear(BACKGROUND_COLOR);

  _context->window->draw(_game_title);

  for (uint32_t i = 0; i < _shape_interactible_bits.size(); i++) {
    _context->window->draw(_shape_interactible_bits[i]);
    _context->window->draw(_text_interactible_bits[i]);
    _context->window->draw(_text_bits_labels[i]);
  }

  _context->window->setView(_context->window->getDefaultView());
  _context->window->display();
}

void InGameState::compose_texts() {
  _game_title.setFont(_context->assets_manager->GetFont("Patrick Hand SC"));
  _game_title.setString(
      "We gladiate, but I guess we really fighting ourselves");
  _game_title.setFillColor(COLOR_ENVIOUS_W);
  _game_title.setCharacterSize(48);
  _game_title.setOrigin(_game_title.getLocalBounds().width / 2,
                        _game_title.getLocalBounds().height / 2);
  _game_title.setPosition(_context->window->getSize().x / 2.0f,
                          _game_title.getLocalBounds().height + 30.0f);
}

void InGameState::compose_interactible_bits() {
  sf::Color slots_color = COLOR_ENVIOUS_B;
  uint32_t slots_count = 8;
  uint32_t slot_border = 10;
  uint32_t slots_width = (128 - slot_border) * slots_count;
  uint32_t slots_offset = (slot_border * slots_count) / 2;

  // Bit Slots
  for (uint32_t i = 0; i < slots_count; i++) {
    sf::Sprite s;

    s.setTexture(_context->assets_manager->GetTexture("T_Card_Square"), true);
    s.setColor(slots_color);
    s.setScale({0.5, 0.5});
    s.setOrigin(0, s.getLocalBounds().height / 2);

    uint32_t pos_x = (slots_width * (slots_count - i)) / slots_count;
    uint32_t pos_y = _context->window->getSize().y - 250;
    s.setPosition(pos_x + slots_offset, pos_y);

    _shape_interactible_bits.push_back(s);
  }

  // Bits Numbers
  for (uint32_t i = 0; i < slots_count; i++) {
    sf::Text t;

    t.setFont(_context->assets_manager->GetFont("Patrick Hand SC"));
    t.setString("0");
    t.setFillColor(slots_color);
    t.setCharacterSize(64);
    t.setOrigin(t.getLocalBounds().width / 2, t.getLocalBounds().height / 2);

    for (auto s : _shape_interactible_bits) {
      uint32_t pos_x = (slots_width * (slots_count - (i - 1))) / slots_count;
      uint32_t pos_y = s.getPosition().y;
      t.setPosition(pos_x - 16, pos_y - 16);
    }

    _text_interactible_bits.push_back(t);
  }

  // Bits label
  for (uint32_t i = 0; i < slots_count; i++) {
    sf::Text t;
    std::stringstream ss;
    ss << power_of_two(i);

    t.setFont(_context->assets_manager->GetFont("Patrick Hand SC"));
    t.setString(ss.str());
    t.setFillColor(slots_color);
    t.setCharacterSize(64);
    t.setOrigin(t.getLocalBounds().width / 2, t.getLocalBounds().height / 2);

    t.setPosition(_text_interactible_bits[i].getPosition().x,
                  _text_interactible_bits[i].getPosition().y + 128);

    _text_bits_labels.push_back(t);
  }
}

uint32_t InGameState::power_of_two(uint32_t exponent) {
  uint32_t power = 1;
  for (uint32_t i = 0; i <= exponent; i++) {
    power = 1 << i;
  }
  return power;
}
