#include <iostream>

#include "../definitions.h"
#include "SFML/Graphics/Text.hpp"
#include "SFML/Window/Event.hpp"
#include "SFML/Window/Keyboard.hpp"
#include "default_state.hpp"
#include "ingame_state.hpp"

DefaultState::DefaultState(GameDataRef data) : _context(data) {}
DefaultState::~DefaultState() { _context.reset(); }

void DefaultState::Initialize() {
  _context->window->setTitle("Current state: DefaultState");

  _context->assets_manager->LoadFont(
      "Patrick Hand SC", "../assets/fonts/Font_OFL_Patrick_Hand_SC.ttf");
  _game_title.setFont(_context->assets_manager->GetFont("Patrick Hand SC"));
  _game_title.setString("Tennis Court");
  _game_title.setFillColor({255, 34, 68, 255});
  _game_title.setCharacterSize(64);
  _game_title.setOrigin(_game_title.getLocalBounds().width / 2,
                        _game_title.getLocalBounds().height / 2);
  _game_title.setPosition(_context->window->getSize().x / 2.0f,
                          _context->window->getSize().y / 2.0f);
}

void DefaultState::HandleInput() {
  sf::Event event;
  while (_context->window->pollEvent(event)) {
    if (event.type == sf::Event::Closed) {
      _context->window->close();
    }

    if (event.type == sf::Event::KeyPressed) {
      if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
        _context->window->close();
      }

      if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) {
        std::cout << "You are already in DefaultState\n";
      }

      if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) {
        _context->state_machine->AddState(
            std::make_unique<InGameState>(_context), true);
      }
    }
  }
}

void DefaultState::Update(sf::Time delta_time) {}

void DefaultState::Draw() {
  _context->window->clear(BACKGROUND_COLOR);

  _context->window->draw(_game_title);

  _context->window->display();
}
