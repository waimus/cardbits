#pragma once

#include <cstdint>
#include <vector>
#include <SFML/Graphics.hpp>

#include "../game.hpp"
#include "../state.hpp"
#include "SFML/Graphics/Text.hpp"

class InGameState : public Seraphus::State {
public:
  InGameState(GameDataRef data);
  ~InGameState();

  /// Called during state entry
  void Initialize() override;

  /// Input-specific block is here
  void HandleInput() override;

  /// Logic update here
  void Update(sf::Time delta_time) override;

  /// Drawing update here
  void Draw() override;

  void Pause() override {}
  void Resume() override {}

private:
  void compose_texts();
  void compose_interactible_bits();
  uint32_t power_of_two(uint32_t power);

private:
  GameDataRef _context;

  sf::Text _game_title;
  std::vector<sf::Sprite> _shape_interactible_bits;
  std::vector<sf::Text> _text_interactible_bits;
  std::vector<sf::Text> _text_bits_labels;
};
