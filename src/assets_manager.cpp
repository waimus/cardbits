#include "assets_manager.hpp"
#include <SFML/Graphics/Font.hpp>

namespace Seraphus {
void AssetsManager::LoadTexture(const char *name, const char *file_path,
                                bool is_repeated) {
  auto t = std::make_unique<sf::Texture>();

  if (t->loadFromFile(file_path)) {
    t->setRepeated(is_repeated);
    this->_textures[name] = std::move(t);
  }
}

sf::Texture &AssetsManager::GetTexture(const char *name) {
  return *(this->_textures.at(name).get());
}

void AssetsManager::LoadFont(const char *name, const char *file_path) {
  auto f = std::make_unique<sf::Font>();

  if (f->loadFromFile(file_path)) {
    this->_fonts[name] = std::move(f);
  }
}

sf::Font &AssetsManager::GetFont(const char *name) {
  return *(this->_fonts.at(name));
}
} // namespace Seraphus
